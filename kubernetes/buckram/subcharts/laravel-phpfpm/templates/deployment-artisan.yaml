apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: {{ template "fullname" . }}-worker
  labels:
    chart: "{{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}"
    release: "{{ .Release.Name }}"
    heritage: "{{ .Release.Service }}"
spec:
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        app: {{ template "fullname" . }}
        tier: "backend"
    spec:
{{- if .Values.workerImage.secrets }}
      imagePullSecrets:
      - name: {{ .Values.workerImage.secrets }}
{{- end }}
      containers:
      - name: {{ .Chart.Name }}-worker
        command:
        - php
        - /var/www/app/artisan
        args:
        - queue:work
        image: "{{ .Values.workerImage.repository }}:{{ .Values.workerImage.tag }}"
        imagePullPolicy: {{ .Values.workerImage.pullPolicy }}
        env:
        - name: APP_LOG
          value: "errorlog"
        - name: APP_KEY
          value: {{ .Values.laravel.app.appKey | quote }}
        - name: APP_URL
          value: {{ .Values.laravel.app.appUrl | quote }}
        - name: QUEUE_DRIVER
          value: {{ .Values.laravel.app.queueDriver | quote }}
        - name: DB_CONNECTION
          value: "pgsql"
        - name: DB_DATABASE
          value: {{ .Values.postgresql.postgresDatabase }}
        - name: DB_HOST
          value: "{{ .Release.Name }}-postgresql"
        - name: MAIL_DRIVER
          value: {{ .Values.laravel.mail.driver | quote }}
        - name: MAIL_HOST
          value: {{ .Values.laravel.mail.host | quote }}
        - name: MAIL_PORT
          value: {{ .Values.laravel.mail.port | quote }}
        - name: MAIL_USERNAME
          value: {{ .Values.laravel.mail.username | quote }}
        - name: MAIL_PASSWORD
          value: {{ .Values.laravel.mail.password | quote }}
        - name: MAIL_FROM_ADDRESS
          value: {{ .Values.laravel.mail.fromAddress | quote }}
        - name: MAIL_FROM_NAME
          value: {{ .Values.laravel.mail.fromName | quote }}
        - name: LINTOL_CKAN_SERVERS
          value: {{ .Values.laravel.services.ckan.servers | quote }}
        - name: LINTOL_CAPSTONE_URL
          value: {{ .Values.laravel.wamp.url | quote }}
        - name: LINTOL_FEATURE_REDIRECTABLE_CONTENT
          value: "{{ if .Values.features.redirectableContent }}1{{ end }}"
        - name: CKAN_URL
          value: {{ .Values.laravel.services.ckan.url | quote }}
        - name: KEY_PATH
          value: "/var/www/app/secrets"
        - name: LINTOL_BLIND_INDEX_KEY
          valueFrom:
            secretKeyRef:
              key: blind-index-key
              name: {{ template "fullname" . }}-env
        - name: GITHUB_CLIENT_ID
          valueFrom:
            secretKeyRef:
              key: github-client-id
              name: {{ template "fullname" . }}-env
        - name: GITHUB_CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              key: github-client-secret
              name: {{ template "fullname" . }}-env
        - name: DB_PASSWORD
          valueFrom:
            secretKeyRef:
              key: postgres-password
              name: "{{ .Release.Name }}-postgresql"
        - name: DB_USERNAME
          value: {{ .Values.postgresql.postgresUser }}
        - name: REDIS_HOST
          value: "{{ .Release.Name }}-redis"
        - name: REDIS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: redis-password
              name: "{{ .Release.Name }}-redis"
        - name: BUCKRAM_INTERNAL_ASSETS
          value: "{{ .Values.internalAssets }}"
        volumeMounts:
        - mountPath: /var/www/app/secrets
          name: keys
          items:
          - key: oauth-public.key
            path: oauth-public.key
          - key: oauth-private.key
            path: oauth-private.key
        - name: laravel-phpfpm-config
          mountPath: /usr/local/etc/php-fpm.d/www.conf
          subPath: www.conf
        resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
      - name: keys
        secret:
          secretName: {{ template "fullname" . }}
      - name: laravel-phpfpm-config
        configMap:
          name: {{ template "fullname" . }}
